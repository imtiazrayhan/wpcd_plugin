<?php
/**
 * Import page content.
 *
 * @since 2.3.2
 */
?>

<div class="wrap">
    <h2><?php echo __( 'Import Coupons from CSV File', 'wpcd-coupon' ); ?></h2>
</div>

<p style="font-size: 16px;"><?php echo __( 'This is a Pro Version feature and only available to Pro Version users. It lets you add bulk of coupons at once from CSV file.', 'wpcd-coupon' ); ?></p>

<p style="font-size: 16px;"><?php echo '<a style="font-weight:bold;" href="' . wcad_fs()->get_upgrade_url() . '">' . __( 'Upgrade to Pro!', 'wpcd-coupon' ) . '</a>' . __( ' or ', 'wpcd-coupon' ) .'<a style="font-weight:bold;" href="' . wcad_fs()->get_trial_url() . '">' . __( 'Start Free Trial!', 'wpcd-coupon' )   . '</a>' . __( ' to start using this feature.', 'wpcd-coupon' ); ?></p>

<p style="font-size: 16px"><?php echo __( 'Alternatively, you can ', 'wpcd-coupon' ) . '<a href="http://wpcouponsdeals.com/knowledgebase/import-coupons-csv-file/">' . __( 'check out Pro Features', 'wpcd-coupon' ) . '</a>' . __( ' and see how it can protect your affiliate sales, generate more revenue.', 'wpcd-coupon' ); ?></p>